#include "stdafx.h"
#include <string>
#include <iostream>



///////////////////////////
//  Base abstract class
//////////////////////////

class Object    
{
public:
	Object(char* address, char* obj_type) : _address(address), _typeName(obj_type) {};
	bool setAddress(char*);
	char* getAddress();
	char* getType() { return _typeName; }
	virtual void showWidget() = 0;
protected:
	char* _address;
	char* _typeName;

};

bool Object::setAddress(char* address)
{
	//if !CHECK(address)
	//return false;
	//else
	_address = address;
	return true;
}

char* Object::getAddress() { return _address; }



///////////////////////////////////////////////
// RealEsate Layer
/////////////////////////////////////////////


template <class T1> class RealEstate : public Object
{
	T1 _property;

public:
	RealEstate(char* address, char* obj_type, T1 init_value) : Object(address, obj_type), _property(init_value) {}
	void setProperty(T1 arg);
	T1 getProperty();
	virtual void showWidget();
};

template <class T1> void RealEstate<T1>::setProperty(T1 arg) { _property = arg; }
template <class T1> T1 RealEstate<T1>::getProperty() { return _property; }
template <class T1> void RealEstate<T1>::showWidget() { ; }

// BUILDING specialized template to prevent creation instance
template<> class RealEstate<int> : public Object {  
	int _property;
public:
	RealEstate(char* address, char* obj_type, int init_value) : Object(address, obj_type), _property(init_value)
	{
		throw "This object is not able to createing";
	};
	void setProperty(int arg) { _property = arg; }
	int getProperty() { return _property; };
	virtual void showWidget() { ; };
	//virtual void BuildingHook() = 0;
};



///////////////////////////////////////////////
// Building Layer
/////////////////////////////////////////////

template <class T2, class T1> class Building : public RealEstate<T1>
{
	T2 _building_property;
public:
	Building(char* address, char* obj_type, T1 init_value, T2 building_property) : RealEstate<T1>(address, obj_type, init_value), _building_property(building_property) {}
	void setBuildingProperty(T2 building_property);
	T2 getBuildingProperty();
	virtual void showWidget();
//protected:
//	virtual void BuildingHook() override;
};


template <class T2, class T1> void Building<T2, T1>::setBuildingProperty(T2 building_property) { _building_propert = building_property; }
template <class T2, class T1> T2 Building<T2, T1>::getBuildingProperty() { return _building_property; }
template <class T2, class T1> void Building<T2, T1>::showWidget() {}
//template<class T1, class T2> void Building<T1, T2>::BuildingHook() {}


// EDUCATIONAL specialized template to prevent creation instance because it have exception in ctor
template <> class Building <char*, int> : public RealEstate<int>
{
	char* _building_property;
public:
	Building(char* address, char* obj_type, int init_value, char* building_property) : RealEstate<int>(address, obj_type, init_value), _building_property(building_property)
	{
		throw "This object is not able to creating";
	}
	void setBuildingProperty(char* building_property) { _building_property = building_property; }
	char* getBuildingProperty() { return _building_property; }
	virtual void showWidget() { ; }
};



///////////////////////////////////////////////
// Educational Layer
/////////////////////////////////////////////

template <class T3, class T2, class T1> class Educational : public Building<T2,T1>
{
	T3 _edu_property;
public:
	Educational(char* address, char* obj_type, T1 init_value, T2 building_property, T3 edu_property) : Building<T2,T1>(address, obj_type, init_value, building_property), _edu_property(edu_property) {}
	void setEduProperty(T3 edu_property);
	T3 getEduProperty();
	virtual void showWidget();
};

template <class T3, class T2, class T1> void Educational<T3, T2, T1>::setEduProperty(T3 edu_property) { _edu_property = edu_property; }
template <class T3, class T2, class T1> T3 Educational<T3, T2, T1>::getEduProperty() { return _edu_property; }
template <class T3, class T2, class T1> void Educational<T3, T2, T1>::showWidget() { ; }



using namespace std;

int main()
{
	int routes[] = { 20, 16, 80 };
	int* pRoutes = routes;
	char** company_list = 0; //dummy

	RealEstate<char*> *pFacility = new RealEstate<char*>("Lermontova, 56", "Faclility", "ooo vysota");
	RealEstate<int*>  *pStopbus = new RealEstate<int*>("Lenina,23", "Stopbus", pRoutes);
	//RealEstate<int>   *pBuilding = new  RealEstate<int>("Lenina,23", "Building", 12);  //EXCEPTION
	
	Building<char*, int> *pHouse = new Building<char*, int>("Lenina,23", "Facility",100,"ZhK Varyag");
	Building<char*, char**> *pOffice = new Building<char*, char**>("Lenina,50", "Facility", company_list, "ooo rosa");

	Educational<char*, int, bool> *pKidgarden = new Educational<char*, int, bool>("Sedova,20","Kidgarden",true,100,"Romashka");


	printf(pFacility->getAddress()); printf("\n");
	printf(pFacility->getProperty()); printf("\n");
	printf(pHouse->getType()); printf("\n");
	printf(pKidgarden->getType());
	
    return 0;
}

